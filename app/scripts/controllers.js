'use strict';

angular.module('newsBlitzWeb1App')

    .controller('LoginController', ['$scope', 'loginService', function ($scope, loginService) {

        $scope.login = function () {
            loginService.loginUser($scope.inputEmail, $scope.inputPassword);
        };


    }])

    //controller for the navbar
    .controller('HeaderController', ['$scope', '$location', '$state', 'localStorageService', function ($scope, $location, $state, localStorageService) {

        $scope.logoutfunc = function () {
            var r = window.confirm("Are you sure you want to log out ?");
            if (r === true) {
                localStorageService.remove("userObj");
                $state.go('login');
                window.alert("Logged out Successfully");

            }
        };

        // to set navbar menu item active status
        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };

        $scope.aboutus = function () {
            window.alert("NewsBlitz version 1.0");
        };



    }])


    .controller('RegistrationController', ['$scope', 'registrationService', function ($scope, registrationService) {

        $scope.registration = function () {


            if ($scope.password !== $scope.password_confirm) {
                window.alert("Password and Confirmation Password do not match");
            }

            else {
                var requestObj = {};

                var subscriptions = [];

                subscriptions.push({ "id": null, "name": null });

                requestObj.firstname = $scope.firstname;
                requestObj.lastname = $scope.lastname;
                requestObj.email = $scope.emailAddress;
                requestObj.password = $scope.password;
                requestObj.subscriptions = subscriptions;

                registrationService.registerUser(JSON.stringify(requestObj));

            }
        };


    }])

    //controller for loading news articles
    .controller('HomeController', ['$scope', 'NewsLoadService', '$state', 'localStorageService', function ($scope, NewsLoadService, $state, localStorageService) {

        //for loading user information
        $scope.getUserInfo = function () {

            if (localStorageService.get("userObj") !== null) {
                $scope.userObj = localStorageService.get("userObj");

                if ($scope.userObj.subscriptions[0].id === null) {
                    window.alert("You have no Subscriptions. Add a few");
                    $state.go('subscriptions');
                }
            }

            else {
                $state.go('login');
            }

            $scope.userSubscriptions = [];
            for (var i = 0, len = $scope.userObj.subscriptions.length; i < len; i++) {

                if ($scope.userObj.subscriptions[i].id !== null && $scope.userObj.subscriptions[i].id !== undefined && $scope.userObj.subscriptions[i].id.length > 3) {

                    $scope.userSubscriptions.push($scope.userObj.subscriptions[i]);

                }
            }


        };


        //for loading image and headline in homepage
        $scope.getImageAndHeadline = function (source) {

            var myDataPromise = NewsLoadService.loadNewsDeferred(source.id);


            myDataPromise.then(function () {

                var newsItem = localStorageService.get(source.id)[0];

                document.getElementById(source.id + "_image").src = newsItem.urlToImage;
                document.getElementById(source.id + "_title").innerText = newsItem.title;
                document.getElementById(source.id + "_description").innerText = newsItem.description;

            });


        };


    }])

    //to load user subscriptions and add/delete subscriptions
    .controller('SubscriptionsController', ['$scope', '$http', 'localStorageService', 'SubscriptionService', function ($scope, $http, localStorageService, SubscriptionService) {

        $scope.loadSubscriptions = function () {

            var myDataPromise = SubscriptionService.loadSourcesDeferred();


            myDataPromise.then(function () {

                $scope.sources = localStorageService.get("AllSources");
            });
        };

        //to set checked/unchecked status in subscriptions page
        $scope.setStatus = function (id) {

            $scope.userSources = localStorageService.get("userObj").subscriptions;

            for (var i = 0, len = $scope.userSources.length; i < len; i++) {
                if (document.getElementById(id) !== null && $scope.userSources[i].id === id) {
                    // document.getElementById($scope.userSources[i].source).checked = true;
                    return true;
                }

            }
            return false;

        };

        //to add/remove subscription
        $scope.updateSubscription = function (id, name) {

            if (document.getElementById(id).checked === true) {
                SubscriptionService.addSource(id, name);
            }
            else {
                SubscriptionService.removeSource(id);
            }
        };

    }])

    // to load articles in detail page
    .controller('DetailController', ['$scope', 'localStorageService', '$stateParams', function ($scope, localStorageService, $stateParams) {

        $scope.loadArticles = function () {

            var source = $stateParams.source;

            var src = source.replace(":", "");

            $scope.userObj = localStorageService.get("userObj");

            $scope.articles = localStorageService.get(src);

        };

    }])
    ;
