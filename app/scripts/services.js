'use strict';
angular.module('newsBlitzWeb1App')


    //service for registration
    .service('registrationService', ['$resource', '$state', 'baseURL', function ($resource, $state, baseURL) {


        this.registerUser = function (requestObj) {

            var User = $resource(baseURL);

            User.save(requestObj, function () {
                window.alert("Registration Successful");
                $state.go('login');
            }, function (error) {
                console.log(error);
            });

        };


    }])

    //service for log in
    .service('loginService', ['$http', '$state', 'localStorageService', 'baseURL', function ($http, $state, localStorageService, baseURL) {


        this.loginUser = function (inputEmail, password) {

            if (localStorageService.get("userObj") !== null) {
                $state.go('home');
            }

            $http({
                method: "GET",
                url: baseURL + "?filter[where][and][0][email]=" + inputEmail + "&filter[and][1][password]=" + password
            }).then(function mySucces(response) {

                if (response.data.length > 0) {
                    localStorageService.set("userObj", response.data[0]);
                    $state.go('home');
                }

                else {
                    window.alert("Invalid email/password. Please try again");
                }

            }, function myError(response) {
                window.alert("Invalid email/password. Please try again");
                console.log(response.statusText);
            });

        };


    }])

    .service('SubscriptionService', ['$http', 'localStorageService', 'baseURL', 'newsSourcesURL', function ($http, localStorageService, baseURL, newsSourcesURL) {


        // load articles for each news source
        this.loadSourcesDeferred = function () {
            return $http({
                method: "GET",
                url: newsSourcesURL
            }).then(function mySucces(response) {
                localStorageService.set("AllSources", response.data.sources);
            }, function myError(response) {
                console.log(response.statusText);
            });

        };

        // add subscription

        this.addSource = function (id, name) {

            var userObj = localStorageService.get("userObj");

            var requestObj = userObj;

            var subscriptions = [];

            var subscription = {};

            subscription.id = id;
            subscription.name = name;

            subscriptions.push(subscription);

            if (requestObj.subscriptions[0].id === null) {
                requestObj.subscriptions = subscriptions;
            }

            else {
                requestObj.subscriptions.push(subscription);
            }
            $http({
                method: "PUT",
                url: baseURL + "/" + userObj.id,
                data: requestObj
            }).then(function mySucces(response) {
                window.alert("Updated Successfully");
                localStorageService.set("userObj", response.data);

            }, function myError(response) {
                console.log(response.statusText);
            });



        };

        //remove subscription
        this.removeSource = function (id) {

            var userObj = localStorageService.get("userObj");

            var requestObj = userObj;

            for (var i = 0, len = userObj.subscriptions.length; i < len; i++) {

                if (id === userObj.subscriptions[i].id) {
                    delete requestObj.subscriptions[i];
                }
            }

            $http({
                method: "PUT",
                url: baseURL + "/" + userObj.id,
                data: requestObj
            }).then(function mySucces(response) {
                window.alert("Removed Successfully");
                localStorageService.set("userObj", response.data);

            }, function myError(response) {
                console.log(response.statusText);
            });



        };


    }])

    .service('NewsLoadService', ['$http', 'localStorageService', 'newsArticlesBaseURL', function ($http, localStorageService, newsArticlesBaseURL) {

        //load news articles
        this.loadNewsDeferred = function (id) {

            var url = newsArticlesBaseURL.replace("{source_name}", id);
            return $http({
                method: "GET",
                url: url
            }).then(function mySucces(response) {
                localStorageService.set(id, response.data.articles);
                return response.data.articles;
            }, function myError(response) {
                console.log(response.statusText);
            });


        };


    }])
    ;
