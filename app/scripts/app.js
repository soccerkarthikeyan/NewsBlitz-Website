'use strict';

/**
 * @ngdoc overview
 * @name newsBlitzWeb1App
 * @description
 * # newsBlitzWeb1App
 *
 * Main module of the application.
 
 */

var newsBlitzWeb1App = angular.module('newsBlitzWeb1App', ['ui.router', 'ngResource', 'LocalStorageModule'])
    .constant("baseURL", "https://newsblitz-db-testing.mybluemix.net/api/usersinfo")
    .constant("newsSourcesURL", "https://newsapi.org/v1/sources?language=en")
    .constant("newsArticlesBaseURL", "https://newsapi.org/v1/articles?source={source_name}&apiKey=ecf0d39691f5462991b503dfcdf0a148");

newsBlitzWeb1App.config(['$stateProvider', '$urlRouterProvider', 'localStorageServiceProvider', function ($stateProvider, $urlRouterProvider, localStorageServiceProvider) {
    $stateProvider

        .state('login', {
            url: '/',
            views: {
                'content': {
                    templateUrl: 'views/login.html',
                    controller: 'LoginController'
                }

            }

        })

        .state('register', {
            url: '/register',
            views: {
                'content': {
                    templateUrl: 'views/register.html',
                    controller: 'RegistrationController'
                }
            }
        })

        .state('home', {
            url: '/home',
            views: {
                'header': {
                    templateUrl: 'views/header.html',
                },

                'content': {
                    templateUrl: 'views/home.html',
                    controller: 'HomeController'
                },
                'footer': {
                    templateUrl: 'views/footer.html',
                }
            }
        })

        .state('subscriptions', {
            url: '/subscriptions',
            views: {
                'header': {
                    templateUrl: 'views/header.html',
                },

                'content': {
                    templateUrl: 'views/subscriptions.html',
                    controller: 'SubscriptionsController'
                },
                'footer': {
                    templateUrl: 'views/footer.html',
                }
            }
        })

        .state('detail', {
            url: '/detail:source',
            views: {
                'header': {
                    templateUrl: 'views/header.html',
                },

                'content': {
                    templateUrl: 'views/detail.html',
                    controller: 'DetailController'
                },
                'footer': {
                    templateUrl: 'views/footer.html',
                }
            }
        });

    $urlRouterProvider.otherwise('/');

    localStorageServiceProvider
        .setPrefix('newsBlitzWeb1App')
        .setStorageType('localStorage')
        .setNotify(true, true);

}])


    ;


